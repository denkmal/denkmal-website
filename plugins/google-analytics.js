import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

export default async (context) => {
  let isDebug = false
  let propertyIds = context.env.GOOGLE_ANALYTICS_IDS.split(',')
  let store = context.store
  let regionSlug = store.getters['regions/selectedRegionSlug']

  let setVariables = [{ field: 'anonymizeIp', value: true }]
  if (regionSlug) {
    setVariables.push({ field: 'dimension1', value: regionSlug })
  }

  Vue.use(VueAnalytics, {
    router: context.app.router,
    id: propertyIds,
    set: setVariables,
    debug: {
      enabled: isDebug,
    },
  })
  let ga = Vue.$ga

  store.watch(
    (state) => state.regions.selectedRegionSlug,
    (regionSlug) => {
      ga.set('dimension1', regionSlug)
    },
  )
}
