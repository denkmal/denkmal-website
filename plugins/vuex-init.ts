export default async (context: any) => {
  await context.store.dispatch('initStore', context)

  if (process.client) {
    let window_any = window as any
    window_any.onNuxtReady(async () => {
      await context.store.dispatch('initStoreAfterMount', context)
    })
  }
}
