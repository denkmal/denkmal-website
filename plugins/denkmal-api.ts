import { DenkmalApi } from '~/assets/api'

export default async ({ env }: any, inject: any) => {
  let apiUrl: string = env.DENKMAL_API_URL
  let denkmalApi = new DenkmalApi(apiUrl)
  inject('denkmalApi', denkmalApi)
}
