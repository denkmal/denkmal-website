import axios, { AxiosInstance, AxiosRequestConfig } from 'axios'
import { Event, Region } from 'assets/types'
import { ASTNode, print as gqlPrint } from 'graphql'
import gql from 'graphql-tag'
import * as de from '@mojotech/json-type-validation'
import { eventDecoder, regionDecoder } from './api/decoders'
import { Cache } from 'async-cacher'
import { isStaticGenerate } from './helpers'

const fragments = {
  DefaultEvent: gql`
    fragment DefaultEvent on Event {
      id
      isPromoted
      description
      hasTime
      from
      until
      eventDay
      links {
        label
        url
      }
      genres {
        name
        category {
          color
        }
      }
      tags
      venue {
        id
        name
        address
        url
        facebookPageId
        latitude
        longitude
        region {
          slug
          dayOffset
          timeZone
        }
      }
    }
  `,
}

// Global shared cache instance
let cache = new Cache()

class DenkmalApi {
  private _axios: AxiosInstance
  private _cache: Cache | any
  private _cacheEnabled: boolean

  constructor(apiUrl: string) {
    this._axios = axios.create({
      baseURL: apiUrl,
    })
    this._cacheEnabled = !!isStaticGenerate
    this._cache = cache
  }

  async fetchRegions(): Promise<Region[]> {
    return this.cache(`fetchRegions`, async () => {
      let query = gql`
        {
          regions {
            slug
            name
            latitude
            longitude
            dayOffset
            timeZone
            email
            suspendedUntil
            venues {
              name
            }
          }
        }
      `
      type ResultType = {
        regions: Region[]
      }
      let decoder: de.Decoder<ResultType> = de.object({
        regions: de.array(regionDecoder),
      })
      let result = await this.sendRequest('get', query, decoder)
      return result.regions
    })
  }

  async fetchEvents(dates: string[]): Promise<{ region: string; date: string; events: { [id: string]: Event } }[]> {
    return this.cache(`fetchEvents(${dates.join(',')})`, async () => {
      let query = gql`
        query getEvents($eventDays: [EventDay!]!) {
          regions {
            slug
            events(eventDays: $eventDays) {
              ...DefaultEvent
            }
          }
        }
        ${fragments.DefaultEvent}
      `
      type ResultType = {
        regions: {
          slug: string
          events: Event[][]
        }[]
      }
      let decoder: de.Decoder<ResultType> = de.object({
        regions: de.array(
          de.object({
            slug: de.string(),
            events: de.array(de.array(eventDecoder)),
          }),
        ),
      })
      let result = await this.sendRequest('get', query, decoder, {
        eventDays: dates,
      })
      let data: any = []
      for (let region of result['regions']) {
        region['events'].forEach((events, index: number) => {
          let eventsObj: { [id: string]: Event } = {}
          for (let event of events) {
            eventsObj[event.id] = event
          }
          data.push({
            region: region.slug,
            date: dates[index],
            events: eventsObj,
          })
        })
      }
      return data
    })
  }

  async fetchEvent(id: string): Promise<Event> {
    return this.cache(`fetchEvent(${id})`, async () => {
      let query = gql`
        query getEvent($id: ID!) {
          event(id: $id) {
            ...DefaultEvent
          }
        }
        ${fragments.DefaultEvent}
      `
      type ResultType = {
        event: Event
      }
      let decoder: de.Decoder<ResultType> = de.object({
        event: eventDecoder,
      })
      let result = await this.sendRequest('get', query, decoder, {
        id,
      })
      return result.event
    })
  }

  async suggestEvent(data: {
    regionSlug: string
    venueName: string
    description: string
    from: string
    until?: string
    links?: string[]
    genres?: string[]
    hasTime?: boolean
  }): Promise<{
    id: string
  }> {
    let query = gql`
      mutation suggestEvent(
        $regionSlug: String!
        $venueName: String!
        $description: String!
        $from: Date!
        $until: Date
        $links: [EventLinkInput]
        $genres: [String]
        $hasTime: Boolean
      ) {
        suggestEvent(
          regionSlug: $regionSlug
          venueName: $venueName
          description: $description
          from: $from
          until: $until
          links: $links
          genres: $genres
          hasTime: $hasTime
        ) {
          id
        }
      }
    `
    type ResultType = {
      suggestEvent: {
        id: string
      }
    }
    let decoder: de.Decoder<ResultType> = de.object({
      suggestEvent: de.object({
        id: de.string(),
      }),
    })
    let result = await this.sendRequest('post', query, decoder, data)
    return result['suggestEvent']
  }

  private cache<T>(key: string, getter: () => T): T {
    if (this._cacheEnabled) {
      return this._cache.get({ key, dataSource: getter })
    } else {
      return getter()
    }
  }

  private async sendRequest<ResultType>(
    method: string,
    query: string | ASTNode,
    decoder: de.Decoder<ResultType>,
    variables?: object,
  ): Promise<ResultType> {
    method = method.toUpperCase()
    if (typeof query === 'object') {
      query = gqlPrint(query)
    }
    let requestData = {
      query,
      variables,
    }
    let config: AxiosRequestConfig = {
      method,
    }
    if (method === 'GET') {
      config.params = requestData
    } else {
      config.data = requestData
    }
    let response = await this._axios.request(config).catch((e) => {
      if (e.response) {
        checkResponseError(e.response.status, e.response.data)
      }
      throw new Error(`API transport error: ${e}`)
    })
    let responseData = response.data
    checkResponseError(200, response.data)
    return decodeGraphqlResponse(responseData, decoder)
  }
}

function checkResponseError(responseStatus: number, responseData: any) {
  let throwError = false
  let responseDataError = undefined

  // HTTP error
  if (responseStatus !== 200) {
    throwError = true
  }

  // GraphQL error
  if (responseData.errors && responseData.errors.length > 0) {
    throwError = true
    let messages = responseData.errors.map((e: any) => e.message)
    responseDataError = messages.join(' | ')
  }

  if (throwError) {
    let details = responseDataError ? responseDataError : responseData
    let message = `API error - HTTP status ${responseStatus}\n\n${details}:`
    throw new Error(message)
  }
}

function decodeGraphqlResponse<ResultType>(responseData: any, decoder: de.Decoder<ResultType>): ResultType {
  let graphqlData = responseData.data
  try {
    return decoder.runWithException(graphqlData)
  } catch (e) {
    let message = `Failed to decode API response: ${e.message}`
    if (e.kind === 'DecoderError') {
      message += `\n`
      message += `\nAt: ${e.at}`
      message += `\nInput: ${JSON.stringify(e.input)}`
    }
    e.message = message
    throw e
  }
}

export { DenkmalApi }
