/**
 * Check if a click event should be handled programmatically by the website.
 * If the click should be handled will return TRUE and prevent the default action.
 *
 * Adopted from vue-router's `guardEvent()`.
 *
 * Returns TRUE if the event should be handled with JS, FALSE otherwise
 */
function shouldHandleClick(e: any) {
  // don't redirect with control keys
  if (e.metaKey || e.altKey || e.ctrlKey || e.shiftKey) return false
  // don't redirect when preventDefault called
  if (e.defaultPrevented) return false
  // don't redirect on right click
  if (e.button !== undefined && e.button !== 0) return false
  // don't redirect if `target="_blank"`
  if (e.currentTarget && e.currentTarget.getAttribute) {
    const target = e.currentTarget.getAttribute('target')
    if (/\b_blank\b/i.test(target)) return false
  }
  // this may be a Weex event which doesn't have this method
  if (e.preventDefault) {
    e.preventDefault()
  }
  return true
}

export { shouldHandleClick }
