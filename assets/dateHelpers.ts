import { DateTime } from 'luxon'
import { Event } from 'assets/types'

export {
  EventDateTime,
  dateUrl,
  eventDateParty,
  eventFromDateTime,
  eventUntilDateTime,
  formatWeekday,
  isValidDateParam,
  parseEventDateTime,
}

type EventDateTime = {
  date: YMD
  time: HM
  timestamp: number
}
type YMD = [number, number, number]
type HM = [number, number]

function eventDateParty(event: Event, locale?: string): DateTime {
  let date = eventFromDateTime(event, locale)
  date = date.minus({ hours: event.venue.region.dayOffset })
  return date.startOf('day')
}

function eventFromDateTime(event: Event, locale?: string): DateTime {
  return parseDate(event.from, event.venue.region.timeZone, locale)
}

function eventUntilDateTime(event: Event, locale?: string): DateTime | undefined {
  if (!event.until) {
    return undefined
  }
  return parseDate(event.until, event.venue.region.timeZone, locale)
}

function parseDate(datetime: string, timeZone: string, locale?: string): DateTime {
  let date = DateTime.fromISO(datetime, { zone: timeZone })
  if (locale) {
    date = date.setLocale(locale)
  }
  return date
}

function parseEventDateTime(datetime: string): EventDateTime {
  let dt = DateTime.fromISO(datetime, { setZone: true })
  return {
    date: [dt.year, dt.month, dt.day],
    time: [dt.hour, dt.minute],
    timestamp: Math.round(dt.toMillis() / 1000),
  }
}

function dateUrl(date: DateTime): string {
  // Not using `DateTime.toFormat()` as a performance optimization
  let year = date.year
  let month = (date.month + '').padStart(2, '0')
  let day = (date.day + '').padStart(2, '0')
  return `${year}-${month}-${day}`
}

function formatWeekday(date: DateTime): string {
  return date
    .toFormat('ccc')
    .replace(/\.$/, '')
    .replace(/^(.)/, (l) => l.toUpperCase())
}

function isValidDateParam(date: string): boolean {
  let dateParsed = dateUrl(DateTime.fromISO(date))
  return dateParsed === date
}
