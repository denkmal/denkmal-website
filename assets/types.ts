import { EventDateTime } from 'assets/dateHelpers'

export { Region, Event, EventLink, EventTime, EventGenre, Venue }

type Region = {
  email: string
  slug: string
  name: string
  latitude: number
  longitude: number
  dayOffset: number
  timeZone: string
  suspendedUntil: string | null
  venues: {
    name: string
  }[]
}

type EventTime = {
  hour: number
  minute: number
}

type Event = {
  id: string
  isPromoted: boolean
  description: string
  from: string
  fromDT: EventDateTime
  until: string | null
  untilDT: EventDateTime | null
  hasTime: boolean
  eventDay: string
  links: EventLink[]
  genres: EventGenre[]
  tags: string[]
  venue: Venue
}

type EventLink = {
  label: string
  url: string
}

type EventGenre = {
  name: string
  category: {
    color: string | null
  }
}

type Venue = {
  id: string
  name: string
  address: string | null
  url: string | null
  facebookPageId: string | null
  latitude: number | null
  longitude: number | null
  region: {
    slug: string
    dayOffset: number
    timeZone: string
  }
}
