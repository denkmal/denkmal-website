import 'jasmine-expect'
import { todayDate } from '../helper/helpers'

describe('Index page', () => {
  const today = todayDate().toFormat('yyyy-MM-dd')

  describe('when no region is selected', () => {
    it('shows list of regions', () => {
      browser.url('/')
      $('.page-regions').waitForDisplayed()
      // The page title is not updated due to https://github.com/nuxt/vue-meta/issues/567
      // The title should be: 'Wähle deine Stadt - Denkmal.org'.
      expect(browser.getTitle()).toEqual('Denkmal.org Eventkalender')
      expect($('body').getText()).toContain('Denkmal.org wird von Locals gemacht.')
    })
  })

  describe('when a selected region is stored', () => {
    beforeAll(() => {
      browser.url('/de/basel')
      $('.page-region-date').waitForDisplayed()
    })

    it('redirects to date-page', () => {
      browser.url('/')
      $('.page-region-date').waitForDisplayed()
      expect(browser.getUrl()).toEndWith(`/de/basel/${today}`)
    })
  })
})
