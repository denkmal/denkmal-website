describe('Add page', () => {
  beforeAll(() => {
    browser.url('/de/add')
    $('.page-add').waitForDisplayed()
  })

  it('has correct title information', () => {
    expect(browser.getTitle()).toEqual('Event eintragen - Denkmal.org')
  })

  it('shows form field errors', () => {
    $('button[type="submit"]').click()

    let $error = browser.$('.FormField-error')
    $error.waitForDisplayed()
    expect($('body').getText()).toContain('Beschreibung ist ein Pflichtfeld.')
  })
})
