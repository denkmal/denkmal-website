import 'jasmine-expect'
import { todayDate } from '../helper/helpers'

// Covering bug https://gitlab.com/denkmal/denkmal-website/issues/118

describe('Date page with favorited venue', () => {
  let today = todayDate()
  let weekday = today.weekday
  let daysUntilFriday = weekday <= 5 ? 5 - weekday : 5 - weekday + 7
  let date = today.plus({ days: daysUntilFriday })

  let favoriteVenueName

  beforeAll(() => {
    // Load date page of next Friday
    browser.url(`/de/basel/${date.toFormat('yyyy-MM-dd')}`)
    browser.$('.page-region-date').waitForDisplayed()

    // Open *last* event
    let $eventLast = browser.$(`.swiper-slide:nth-child(${daysUntilFriday + 1}) .DayEvents-list-item:last-child .Event`)
    favoriteVenueName = $eventLast.$('.Event-meta-venue').getText()
    $eventLast.click()
    browser.$('.page-region-date-event').waitForDisplayed()

    // Add venue to favorites
    browser.$(`//*[text()="Lieblingsort"]`).click()
    browser.$('.EventDetailsMeta--favorite-venue').waitForDisplayed()

    // Back to event list
    browser.$(`//*[text()="Zurück"]`).click()
    browser.$('.page-region-date').waitForDisplayed()
  })

  it('lists the favorited venue first', () => {
    let $eventFirst = browser.$(
      `.swiper-slide:nth-child(${daysUntilFriday + 1}) .DayEvents-list-item:first-child .Event`,
    )
    let firstVenueName = $eventFirst.$('.Event-meta-venue').getText()
    expect(firstVenueName).toEqual(favoriteVenueName)
    expect(firstVenueName.length).toBeGreaterThan(0)
  })

  it('allows to navigate to different page', () => {
    browser.$('button.hamburger').click()
    let $linkAbout = browser.$(`//*[text()="Über uns"]`)
    $linkAbout.waitForDisplayed()
    $linkAbout.click()
    browser.$('.page-about').waitForDisplayed()
  })
})
