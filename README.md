denkmal-website
===============

Website for Denkmal.org - Mobile-first event calendar using Nuxt.js.

Development
-----------

Install dependencies:
```sh
yarn install
```

Serve with hot reload at http://localhost:3000:
```sh
yarn run dev
```

### NodeJS version

Note: this app uses a specific version of NodeJs (currently NodeJS v13).
If you don't want to install it, you can use the pre-configured docker-compose container:
```
docker-compose run nodejs
```
(or `podman-compose ...`)

### Try out static/generated version

Generate the static version:
```
yarn run generate
```

Then serve at http://localhost:3000:
```
yarn run serve
```

### Webdriver tests

Integration tests with [WebdriverIO](https://webdriver.io/) run in a headless browser.  

Tests can either run against the real production API, or against a mocked/recorded API (using [proxay](https://github.com/airtasker/proxay)).
Behaviour is configured using these env vars:
- `DENKMAL_API_URL`: URL of the [Denkmal backend API](https://gitlab.com/denkmal/denkmal-api)
- `DENKMAL_MOCK_NOW` Optional ISO8601 date string that will be used to determine what is "now".

#### Run Tests against Mocked API

Start chromedriver + proxay:
```
docker-compose up
```
(or `podman-compose up`)

Start webserver:
```
yarn run serve
```

Configure the mock API URL, and the date when the mocks were recorded:
```
export DENKMAL_API_URL='http://localhost:8010/graphql'
export DENKMAL_MOCK_NOW='2019-07-01T20:00:00+02:00'
```

Generate the app:
```
yarn run generate
```

Run tests:
```
yarn run specs
```

#### Update API Mocks

Remove existing mocks:
```
rm test/proxay_tapes/default.yml
```

Modify the "proxay_denkmal_api" service in "docker-compose.yml", and uncomment the command with `--mode replay`.
Then start chromedriver + proxay:
```
docker-compose up
```
(or `podman-compose up`)

Start webserver:
```
yarn run serve
```

Generate the app
```
yarn run generate
```

Run tests:
```
yarn run specs
```

Finally check these files:
- `.gitlab-ci.yml`: update `DENKMAL_MOCK_NOW`
- `README.md`: update `DENKMAL_MOCK_NOW`
- `test/proxay_tapes/default.yml`: commit the recorded requests


#### Run Tests against Real API

Start chromedriver
```
docker-compose up chromedriver
```

Generate the app
```
yarn run generate
```

Run tests:
```
yarn run specs
```


#### Run a single spec test
```
yarn run specs --spec test/specs/<SPEC-FILE>.js
```

#### Run Tests like on CI
To run the tests like on CI in a single command, [gitlab-runner](https://docs.gitlab.com/runner/install/) can be used:
```
gitlab-runner exec docker test --docker-privileged
```

### Code Formatting and Linting

We use *prettier* for code formatting.

You can check if styles are correctly formatted:
```
yarn run lint
```

And apply formatting with:
```
yarn run lint-fix
```

### Translations

Check missing and duplicated translation keys
```
yarn vue-i18n-extract
```

### Code styles

https://vuejs.org/v2/style-guide/

### Nuxt Page Lifecycle

There are three distinct lifecycle paths for loading Nuxt pages.
Note that `initStore()` is provided by a custom plugin.

#### Initial load of server-side generated page (Pre Rendering)

1. Server: Store's `initStore()`
2. Server: Page's `fetch()` and `asyncData()`
3. Server: Page's `created()`
4. Client: Page's `created()`
5. Client: Page's `mounted()`
6. Client: Store's `initStoreAfterMount()`

#### Initial load of a not-generated page (SPA)

1. Client: Store's `initStore()`
2. Client: Page's `fetch()` and `asyncData()`
3. Client: Page's `created()`
4. Client: Page's `mounted()`
5. Client: Store's `initStoreAfterMount()`

Unfortunately in the SPA case there's no way to inject data from the server-side.
See [this question](https://cmty.app/nuxt/nuxt.js/issues/c8598).

#### Navigating to another page:

1. Client: Page's `fetch()` and `asyncData()`
2. Client: Page's `created()`
3. Client: Page's `mounted()`

### Update nuxt

When updating nuxt, make sure to update @nuxt/vue-app patch. `npx patch-package @nuxt/vue-app`

Community
---------

[Denkmalmit.org](https://denkmalmit.org).
