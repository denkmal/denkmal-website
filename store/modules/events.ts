import Vue from 'vue'
import { Module } from 'vuex'

import { AllState } from 'store'
import { Event } from 'assets/types'
import { DenkmalApi } from 'assets/api'
import { ascend, sortWith } from 'ramda'
import { AtomicReactive, atomicReactiveGet, atomicReactiveInit, atomicReactiveSet } from '../atomicReactive'

export { EventsModule, EventsState }

type EventsByRegionAndDate = {
  [region: string]: {
    [date: string]: {
      [id: string]: Event
    }
  }
}

interface EventsState {
  eventsById: { [id: string]: Event }
  eventsByRegionAndDate: AtomicReactive<EventsByRegionAndDate>
}

let EventsModule: Module<EventsState, AllState> = {
  namespaced: true,
  state: (): EventsState => ({
    eventsById: {},
    eventsByRegionAndDate: atomicReactiveInit({}),
  }),
  mutations: {
    setEventById(state, event: Event) {
      Vue.set(state.eventsById, event.id, event)
    },
    setEventsByDates(state, eventDates: { region: string; date: string; events: { [id: string]: Event } }[]) {
      let eventsByRegionAndDate = atomicReactiveGet(state.eventsByRegionAndDate)
      for (let item of eventDates) {
        if (!eventsByRegionAndDate.hasOwnProperty(item.region)) {
          eventsByRegionAndDate[item.region] = {}
        }
        eventsByRegionAndDate[item.region][item.date] = item.events
      }
      atomicReactiveSet(state.eventsByRegionAndDate, eventsByRegionAndDate)
    },
  },
  actions: {
    async setEventById({ commit }, event: Event) {
      commit('setEventById', event)
    },
    async setEventsByDates(
      { commit },
      eventDates: { region: string; date: string; events: { [id: string]: Event } }[],
    ) {
      commit('setEventsByDates', eventDates)
    },
    async getEventById({ commit, state }, id: string): Promise<Event> {
      // Lookup in `eventsById`
      if (state.eventsById[id]) {
        return state.eventsById[id]
      }

      // Lookup in `eventsByDate`
      let eventsByRegionAndDate = atomicReactiveGet(state.eventsByRegionAndDate)
      for (let region in eventsByRegionAndDate) {
        for (let date in eventsByRegionAndDate[region]) {
          if (eventsByRegionAndDate[region][date][id]) {
            return eventsByRegionAndDate[region][date][id]
          }
        }
      }

      // Load from API
      let denkmalApi = (this as any).$denkmalApi as DenkmalApi
      let event = await denkmalApi.fetchEvent(id)
      commit('setEventById', event)
      return event
    },
    async ensureEventsByDates(
      { commit, state },
      { datesEnsure, datesLoad, force }: { datesEnsure: string[]; datesLoad?: string[]; force?: boolean },
    ) {
      let eventsByRegionAndDate = atomicReactiveGet(state.eventsByRegionAndDate)
      let eventsByDate = Object.values(eventsByRegionAndDate).find((_) => true)
      let availableDates = new Set(eventsByDate ? Object.keys(eventsByDate) : [])

      let hasMissingDates = [...datesEnsure].filter((d) => !availableDates.has(d)).length > 0
      if (force || hasMissingDates) {
        if (!datesLoad) {
          datesLoad = datesEnsure
        }
        let denkmalApi = (this as any).$denkmalApi as DenkmalApi
        const eventDates = await denkmalApi.fetchEvents([...datesLoad])
        commit('setEventsByDates', eventDates)
      }
    },
    async ensureEventsByWeek({ dispatch, rootGetters }, force?: boolean) {
      const datesEnsure: string[] = rootGetters['datetime/weekDates']
      // Load a day before and after, because the `dayOffset` will change when the region is set
      const datesLoad: string[] = rootGetters['datetime/weekDatesExtended']
      await dispatch('ensureEventsByDates', { datesEnsure, datesLoad, force })
    },
    async loadEventsByWeek({ dispatch }) {
      await dispatch('ensureEventsByWeek', true)
    },
  },
  getters: {
    eventsByDate: (state, _getters, rootState, rootGetters) => (date: string): Event[] => {
      let regionSlug = rootGetters['regions/selectedRegionSlug']
      if (regionSlug === undefined) {
        throw new Error('Cannot get events, no region is set')
      }
      let eventsByRegionAndDate = atomicReactiveGet(state.eventsByRegionAndDate)
      let eventsByDate = eventsByRegionAndDate[regionSlug]
      if (!eventsByDate) {
        throw new Error(`Cannot get events, not loaded for region '${regionSlug}'`)
      }
      if (!eventsByDate.hasOwnProperty(date)) {
        // We might not have events for `date` yet, if the date has just changed (via datetime store)
        return []
      }
      const events = Object.values(eventsByDate[date])

      const sorting = rootState.userPreferences.eventListSorting
      const favoriteVenues = rootState.userPreferences.favoriteVenues
      return sortEvents(events, sorting, favoriteVenues)
    },
    eventsByWeek: (_state, getters, _rootState, rootGetters): { [date: string]: Event[] } => {
      const dates: string[] = rootGetters['datetime/weekDates']
      let result: { [date: string]: Event[] } = {}
      for (let date of dates) {
        result[date] = getters.eventsByDate(date)
      }
      return result
    },
  },
}

function sortEvents(events: Event[], by: string, favoriteVenues: string[]): Event[] {
  switch (by) {
    case 'alphabetically':
      return sortWith([
        ascend((e: Event) => (favoriteVenues.includes(e.venue.id) ? 1 : 2)),
        ascend((e: Event) => e.venue.name.toLowerCase()),
        ascend((e: Event) => e.fromDT.timestamp),
        ascend((e: Event) => e.id),
      ])(events)
    case 'proximity':
      return events
    default:
      throw new Error(`Unknown sort order '${by}'`)
  }
}
