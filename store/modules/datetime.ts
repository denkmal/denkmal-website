import { Module } from 'vuex'
import { AllState } from 'store'
import { identity, times } from 'ramda'
import { DateTime } from 'luxon'
import { dateUrl } from 'assets/dateHelpers'
import { dateTimeNow } from '~/assets/helpers'

export { DatetimeModule, DatetimeState }

interface DatetimeState {
  now: string // ISO 8601
}

let DatetimeModule: Module<DatetimeState, AllState> = {
  namespaced: true,
  state: (): DatetimeState => ({
    now: dateTimeNow().toISO(),
  }),
  getters: {
    now: (state, getters, rootState): DateTime => {
      let now = DateTime.fromISO(state.now)
      if (getters.timeZone) {
        now = now.setZone(getters.timeZone)
      }
      if (rootState.i18n.language) {
        now = now.setLocale(rootState.i18n.language)
      }
      return now
    },
    timeZone: (_state, _getters, _rootState, rootGetters): string | undefined => {
      let selectedRegion = rootGetters['regions/selectedRegion']
      if (selectedRegion) {
        return selectedRegion.timeZone
      }
      return undefined
    },
    dayOffset: (_state, _getters, _rootState, rootGetters): number => {
      let selectedRegion = rootGetters['regions/selectedRegion']
      if (selectedRegion) {
        return selectedRegion.dayOffset
      }
      return 0
    },
    todayDateTime: (_state, getters): DateTime => {
      return getters.now.minus({ hours: getters.dayOffset })
    },
    todayDate: (_state, getters): string => {
      return dateUrl(getters.todayDateTime)
    },
    weekDateTimes: (_state, getters): DateTime[] => {
      let today = getters.todayDateTime
      return times(identity, 7).map((i) => today.plus({ days: i }))
    },
    weekDates: (_state, getters): string[] => {
      return getters.weekDateTimes.map(dateUrl)
    },
    weekDatesExtended: (_state, getters): string[] => {
      let start = getters.todayDateTime.minus({ days: 1 })
      return times(identity, 9)
        .map((i) => start.plus({ days: i }))
        .map(dateUrl)
    },
    dateIsInCurrentWeek: (_state, getters) => (date: string): boolean => {
      return getters.weekDates.includes(date)
    },
    nowIsNight: (_state, getters): boolean => {
      let currentHour = getters.now.get('hour')
      return currentHour >= 22 || currentHour < 6 // We assume best UX is achieved by hard-coding the night time hours
    },
  },
  mutations: {
    updateTime(state) {
      state.now = dateTimeNow().toISO()
    },
  },
  actions: {
    async startInterval({ commit }) {
      if (process.client) {
        setInterval(() => {
          commit('updateTime')
        }, 1000 * 60)
      }
    },
  },
}
