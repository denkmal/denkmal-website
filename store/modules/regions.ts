import { DenkmalApi } from '~/assets/api'
import { Module } from 'vuex'
import { AllState } from 'store'
import { Region } from '~/assets/types'
import { getField } from 'vuex-map-fields'
import storage from 'local-storage-fallback'
import { DateTime } from 'luxon'

export { RegionsModule, RegionsState }

interface RegionsState {
  regions: Region[]
  selectedRegionSlug?: string
}

let RegionsModule: Module<RegionsState, AllState> = {
  namespaced: true,
  state: (): RegionsState => ({
    regions: [],
    selectedRegionSlug: undefined,
  }),
  mutations: {
    setRegions(state, regions: Region[]) {
      state.regions = regions
    },
    setSelectedRegionSlug(state, slug: string) {
      if (!state.regions.find((r) => r.slug === slug)) {
        throw new Error(`Cannot find region '${slug}'`)
      }
      state.selectedRegionSlug = slug
      if (process.client) {
        storage.setItem('selectedRegionSlug', slug)
      }
    },
  },
  getters: {
    getField,
    selectedRegionSlug: (state): string | undefined => {
      return state.selectedRegionSlug
    },
    selectedRegion: (state): Region | undefined => {
      if (!state.selectedRegionSlug) {
        return undefined
      }
      return state.regions.find((r) => r.slug === state.selectedRegionSlug)
    },
    isSelectedRegionTsri: (_state, getters) => {
      return getters.selectedRegionSlug === 'tsri'
    },
    suspendedUntil: (_state, getters): undefined | DateTime => {
      let selectedRegion: Region | undefined = getters.selectedRegion
      if (selectedRegion === undefined) {
        return
      }
      let suspendedUntil = selectedRegion.suspendedUntil
      if (suspendedUntil === null) {
        return
      }
      return DateTime.fromISO(suspendedUntil)
    },
    isValidRegionSlug: (state) => (slug: string) => {
      return !!state.regions.find((r) => r.slug === slug)
    },
  },
  actions: {
    async loadRegions({ commit }) {
      let denkmalApi = (this as any).$denkmalApi as DenkmalApi
      let regions = await denkmalApi.fetchRegions()
      commit('setRegions', regions)
    },

    setSelectedRegionBySlug({ commit }, slug: string) {
      commit('setSelectedRegionSlug', slug)
    },

    loadSelectedRegionFromStorage({ commit, state }) {
      if (process.client && !state.selectedRegionSlug) {
        let slug = storage.getItem('selectedRegionSlug')
        if (slug) {
          commit('setSelectedRegionSlug', slug)
        }
      }
    },
  },
}
