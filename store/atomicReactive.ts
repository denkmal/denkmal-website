/**
 * Data structure to store an atomically-reactive object in Vuex, meaning
 * that (nested) properties are not made reactive, but instead the whole object
 * changes atomically, based on a "version".
 *
 * Improves performance when storing large, complex objects that mostly change
 * atomically.
 */

export { AtomicReactive, atomicReactiveInit, atomicReactiveSet, atomicReactiveGet }

type AtomicReactive<T> = {
  version: number
  data: {
    [key: number]: T
    _isVue: boolean // Avoid making data reactive (https://stackoverflow.com/a/56091103/3090404)
  }
}

function atomicReactiveInit<T>(data: T): AtomicReactive<T> {
  return {
    version: 0,
    data: { 0: data, _isVue: true },
  }
}

function atomicReactiveSet<T>(obj: AtomicReactive<T>, data: T) {
  delete obj.data[obj.version]
  let versionNew = obj.version + 1
  obj.data[versionNew] = data
  obj.version = versionNew
}

function atomicReactiveGet<T>(obj: AtomicReactive<T>): T {
  return obj.data[obj.version]
}
